#include <cstdlib>
#include <ctime>
#include <future>
#include <thread>
#include <mutex>
#include "matrix.h"

int Qmatrix::_mul(size_t i, size_t j, size_t size, Qmatrix &v) {
    int sum = 0;
    for (size_t k = 0; k < size; k++) {
        sum += (_vec->at(i).at(k) * v._vec->at(k).at(j));
    }
    return sum;
}

Qmatrix::Qmatrix(const Qmatrix &val) {
    _vec = new std::vector<std::vector<int>>(*val._vec);
}

Qmatrix &Qmatrix::operator=(const Qmatrix &v) {
    if (&v == this) {
        return *this;
    }

    delete _vec;

    _vec = new std::vector<std::vector<int>>(*v._vec);

    return *this;
}

void Qmatrix::RandomSet() {
    for (auto &i: *_vec) {
        for (auto &j:i) {
            j = rand() % 100 + 1;
        }
    }
}

void Qmatrix::Print() {
    for (const auto &i: *_vec) {
        for (auto &j:i) {
            std::cout << j << " ";
        }
        std::cout << std::endl;
    }
}

Qmatrix Qmatrix::mul(Qmatrix &v) {
    size_t sz = _vec->size();
    Qmatrix tmp(sz);

    for (size_t i = 0; i < sz; ++i) {
        for (size_t j = 0; j < sz; ++j) {
            tmp._vec->at(i).at(j) = _mul(i, j, sz, v);
        }
    }

    return tmp;
}

Qmatrix Qmatrix::async_mul(Qmatrix &v) {
    size_t sz = _vec->size();
    Qmatrix tmp(sz);
    std::vector<std::future<int>> threads;

    for (size_t i = 0; i < sz; ++i) {
        for (size_t j = 0; j < sz; ++j) {
            threads.emplace_back(std::async(std::launch::async, &Qmatrix::_mul, this, i, j, sz, std::ref(v)));
        }
    }

    auto it = threads.begin();
    for (size_t i = 0; i < sz; ++i) {
        for (size_t j = 0; j < sz; ++j) {
            tmp._vec->at(i).at(j) = it->get();
            ++it;
        }
    }
    return tmp;
}

void Qmatrix::_thread_mul(Qmatrix &v, size_t fo, size_t to, size_t size, Qmatrix &res) {

    for (size_t i = fo; i < to; ++i) {
        for (size_t j = 0; j < size; ++j) {
            res._vec->at(i).at(j) = _mul(i, j, size, v);
        }
    }
}

Qmatrix Qmatrix::thread_mul(Qmatrix &v, size_t num_of_thread) {
    size_t sz = _vec->size();
    Qmatrix tmp(sz);
    const size_t size_of_part = sz / num_of_thread;
    std::mutex mut1, mut2;

    size_t to = 0;
    size_t fo = 0;

    std::vector<std::thread> threads;

    for (size_t i = 0; i < num_of_thread; ++i) {
        to += size_of_part;
        threads.emplace_back(
                std::thread(
                        &Qmatrix::_thread_mul, this, std::ref(v), fo, to, sz, std::ref(tmp)
                )
        );
        fo = to;
    }

    for (auto &i:threads) {
        i.join();
    }

    return tmp;
}