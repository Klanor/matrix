#ifndef MATRIX_THREAD_MATRIX_H
#define MATRIX_THREAD_MATRIX_H

#include <vector>
#include <iostream>

class Qmatrix {
    std::vector<std::vector<int>> *_vec;

    int _mul(size_t i, size_t j, size_t size, Qmatrix &v);

    void _thread_mul(Qmatrix &v, size_t fo, size_t to, size_t size, Qmatrix &res);

public:
    Qmatrix() : _vec(nullptr) {};

    explicit Qmatrix(int size) : _vec(new std::vector<std::vector<int>>(size, std::vector<int>(size))) {};

    Qmatrix(const Qmatrix &val);

    Qmatrix &operator=(const Qmatrix &v);

    void RandomSet();

    void Print();

    Qmatrix mul(Qmatrix &v);

    Qmatrix async_mul(Qmatrix &v);

    Qmatrix thread_mul(Qmatrix &v, size_t num_of_thread);

    ~Qmatrix() { delete _vec; }
};

#endif //MATRIX_THREAD_MATRIX_H
