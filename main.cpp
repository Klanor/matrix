#include "matrix.h"
#include <chrono>

void Test() {

}

int main() {
    size_t sz = 1000;
    size_t num_of_thread = 8;

    Qmatrix res;
    Qmatrix tmp(sz);
    Qmatrix m(tmp);
    m.RandomSet();
    tmp.RandomSet();
    //std::cout<<"M1"<<std::endl;
    //tmp.Print();
    //std::cout<<"M2"<<std::endl;
    //m.Print();
    std::cout << "Start calculating with matrix " << sz << "x" << sz << std::endl;

    auto start = std::chrono::steady_clock::now();
    res = tmp.mul(m);
    auto stop = std::chrono::steady_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "result mul in one thread: " << time.count() << std::endl;

    start = std::chrono::steady_clock::now();
    res = tmp.async_mul(m);
    stop = std::chrono::steady_clock::now();
    time = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "result mul with using std::async: " << time.count() << std::endl;

    start = std::chrono::steady_clock::now();
    res = tmp.thread_mul(m, num_of_thread);
    stop = std::chrono::steady_clock::now();
    time = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "result mul with using " << num_of_thread << " thread's: " << time.count() << std::endl;


    //std::cout<<"M3"<<std::endl;
    //res.Print();

    return 0;
}
